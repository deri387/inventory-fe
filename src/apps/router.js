import React, { lazy, Suspense } from "react";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import helper from "../utils/helper";

const LoginPage = lazy(() => import("../pages/LoginPage"));
const DashboardPage = lazy(() => import("../pages/DashboardPage"));
const BlankNotAccess = lazy(() => import("../components/Blank/BlankNotAccess"));

const CategoryPage = lazy(() => import("../pages/CategoryPage"));
const LocationPage = lazy(() => import("../pages/LocationPage"));
const InventoryPage = lazy(() => import("../pages/InventoryPage"));
const TransactionPage = lazy(() => import("../pages/TransactionPage"));

const PageLoader = () => (
  <div id="loader">
    <div className="loadingio-spinner-rolling-3kjtqyynjid">
      <div className="ldio-ucwibazh2i9">
        <div></div>
      </div>
    </div>
  </div>
);
const PublicRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        !helper.isAuthenticated() ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/dashboard",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

const PrivateRoute = ({ title, children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        helper.isAuthenticated() ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

function Router() {
  const routeList = [
    {
      path: "/category",
      component: <CategoryPage />,
    },
    {
      path: "/location",
      component: <LocationPage />,
    },
    {
      path: "/inventory",
      component: <InventoryPage />,
    },
    {
      path: "/transaction",
      component: <TransactionPage />,
    },
  ];

  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/dashboard" />} />
        <PublicRoute path="/login">
          <LoginPage />
        </PublicRoute>
        <PrivateRoute path="/dashboard">
          <DashboardPage />
        </PrivateRoute>

        {routeList.map((v, i) => (
          <PrivateRoute path={v.path} key={i}>
            {helper.isAuthenticated() ? v.component : <BlankNotAccess />}
          </PrivateRoute>
        ))}
      </Switch>
    </Suspense>
  );
}
export default withRouter(Router);
