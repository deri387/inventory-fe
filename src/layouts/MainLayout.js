import { createElement, useEffect, useState } from "react";
import Layout from "antd/es/layout";
import Menu from "antd/es/menu";
import Dropdown from "antd/es/dropdown";
import Modal from "antd/es/modal";
import Button from "antd/es/button";
import Avatar from "antd/es/avatar";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  LogoutOutlined,
  HomeOutlined,
  TagOutlined,
  ExclamationCircleOutlined,
  UserOutlined,
  LockOutlined,
  DatabaseOutlined,
  FormOutlined,
  EnvironmentOutlined,
  ReconciliationOutlined,
} from "@ant-design/icons";
import "./MainLayout.css";
import logoWhite from "../assets/img/logo.png";
import helper from "../utils/helper";
import history from "../utils/history";
// import ResetForm from "../components/Forms/ResetForm";
import http from "../utils/http";
import ChangePassword from "../components/Forms/ChangePasswordForm";
import ProfileForm from "../components/Forms/ProfileForm";
const getIsMobile = () => window.innerWidth <= 768;

function useIsMobile() {
  const [isMobile, setIsMobile] = useState(getIsMobile());

  useEffect(() => {
    const onResize = () => {
      setIsMobile(getIsMobile());
    };

    window.addEventListener("resize", onResize);

    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  return isMobile;
}
function MainLayout(props) {
  const isMobile = useIsMobile();
  const [collapsed, setCollapsed] = useState(false);
  const [visiblePassword, setVisiblePassword] = useState(false);
  const [visibleProfile, setVisibleProfile] = useState(false);

  const { Header, Sider, Content } = Layout;
  const { children, title } = props;
  const { SubMenu } = Menu;
  //const storage = JSON.parse(localStorage.getItem("db"));
  useEffect(() => {
    if (isMobile) {
      setCollapsed(true);
    } else {
      setCollapsed(false);
    }
  }, [isMobile]);

  return (
    <Layout>
      <Sider
        theme="light"
        trigger={null}
        collapsible
        collapsed={collapsed}
        collapsedWidth={isMobile ? 0 : 80}
        breakpoint="lg"
        style={{ overflowY: "auto", height: window.innerHeight }}
      >
        <div className="logo">
          <img src={logoWhite} alt="Logo" />
        </div>
        <Menu
          theme="light"
          mode="inline"
          defaultSelectedKeys={[window.location.pathname]}
          onClick={(e) => {
            if (e.key === "/logout") {
              localStorage.removeItem("db");
              window.location.reload(true);
            } else {
              history.push(e.key);
            }
          }}
        >
          <Menu.Item key="/dashboard" icon={<HomeOutlined />}>
            Dashboard
          </Menu.Item>
          <SubMenu
            icon={<DatabaseOutlined />}
            title={"Master data"}
            key="#master-data"
          >
            <Menu.Item key="/category" icon={<TagOutlined />}>
              Kategori
            </Menu.Item>
            <Menu.Item key="/location" icon={<EnvironmentOutlined />}>
              Lokasi
            </Menu.Item>
            <Menu.Item key="/inventory" icon={<ReconciliationOutlined />}>
              Peralatan
            </Menu.Item>
          </SubMenu>

          <Menu.Item key="/transaction" icon={<FormOutlined />}>
            Transaksi
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout
        className="site-layout"
        style={{
          height: props.height ? props.height : "100%",
          overflowY: props.overflow ? props.overflow : "auto",
        }}
      >
        <Header
          className="site-layout-background flex align-center shadow"
          style={{
            padding: 0,
            marginBottom: 20,
            position: "sticky",
            top: 0,
            zIndex: 2,
          }}
        >
          {createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: "trigger",
            onClick: () => setCollapsed(!collapsed),
          })}
          <span
            style={{
              marginLeft: !isMobile ? 0 : "unset",
              fontSize: !isMobile ? 17 : 15,
              marginRight: 5,
            }}
          >
            {helper.truncString(title, 30, "...")}
          </span>
          {(collapsed && isMobile) || !isMobile ? (
            <div
              style={{ marginLeft: "auto", marginRight: 30, cursor: "pointer" }}
            >
              <Dropdown
                overlay={
                  <Menu
                    onClick={(e) => {
                      if (e.key === "/logout") {
                        Modal.confirm({
                          title: "Logout",
                          icon: <ExclamationCircleOutlined />,
                          content: "Anda yakin ingin logout?",
                          okText: "Ya",
                          cancelText: "Tidak",
                          onOk: () => {
                            http.get("/v1/logout").then((res) => {
                              if (res) {
                                localStorage.removeItem("db");
                                window.location.reload(true);
                              }
                            });
                          },
                        });
                      } else if (e.key === "/reset-password") {
                        setVisiblePassword(true);
                      } else {
                        setVisibleProfile(true);
                      }
                    }}
                  >
                    <Menu.Item key="/profile" icon={<UserOutlined />}>
                      Ubah profile
                    </Menu.Item>
                    <Menu.Item key="/reset-password" icon={<LockOutlined />}>
                      Ubah password
                    </Menu.Item>
                    <Menu.Divider />
                    <Menu.Item key="/logout" icon={<LogoutOutlined />}>
                      Logout
                    </Menu.Item>
                  </Menu>
                }
                trigger={["click"]}
              >
                <a
                  href="/#"
                  className="ant-dropdown-link flex align-center"
                  onClick={(e) => e.preventDefault()}
                >
                  <div className="flex flex-column" style={{ marginRight: 10 }}>
                    <span
                      style={{
                        fontSize: 12,
                        color: "#6e6b7b",
                        fontWeight: "bold",
                        textAlign: "right",
                      }}
                    >
                      {helper.isAuthenticated()?.user?.name}
                    </span>
                    <span
                      style={{
                        fontSize: 10,
                        color: "#6e6b7b",
                        textAlign: "right",
                      }}
                    >
                      {helper.isAuthenticated()?.user?.email}
                    </span>
                  </div>
                  <Avatar icon={<UserOutlined />} />
                </a>
              </Dropdown>
            </div>
          ) : null}
        </Header>
        <Content
          className="site-layout-background"
          style={{
            minHeight: 280,
            background: "unset",
            position: "relative",
          }}
        >
          {children}
          <div style={{ paddingBottom: 50 }} />
          <div
            style={{
              textAlign: "center",
              color: "#212121",
              bottom: 10,
              right: 20,
              fontSize: 12,
              paddingBottom: 20,
            }}
          >
            Copyright &copy;{new Date().getFullYear()} RPL SMKN 1 Katapang | All
            Right Reserved
          </div>
        </Content>
        <Modal
          centered
          visible={visiblePassword}
          title={"Edit Password"}
          onCancel={async () => {
            await setVisiblePassword(false);
          }}
          footer={[
            <Button
              key="back"
              onClick={async () => {
                await setVisiblePassword(false);
              }}
            >
              Tutup
            </Button>,
          ]}
        >
          <ChangePassword
            handleClose={async () => {
              await setVisiblePassword(false);
            }}
            email={helper.isAuthenticated()?.user.email}
            authenticated={true}
          />
        </Modal>
        <Modal
          centered
          visible={visibleProfile}
          title={"Edit Profile"}
          onCancel={async () => {
            await setVisibleProfile(false);
          }}
          footer={[
            <Button
              key="back"
              onClick={async () => {
                await setVisibleProfile(false);
              }}
            >
              Tutup
            </Button>,
          ]}
        >
          <ProfileForm
            handleClose={async () => {
              await setVisibleProfile(false);
            }}
          />
        </Modal>
      </Layout>
    </Layout>
  );
}
export default MainLayout;
