import Card from "antd/es/card";
import Carousel from "antd/es/carousel";
import Row from "antd/es/row";
import Col from "antd/es/col";
import { ReconciliationOutlined } from "@ant-design/icons";
import logo from "../assets/img/logo.png";
import loginLeft from "../assets/img/login-left.jpeg";
import "./FullLayout.css";
function FullLayout(props) {
  const { children, title } = props;
  return (
    <div
      className="full-layout"
      style={{
        height: "100%",
      }}
    >
      <Row style={{ height: "100%" }}>
        <Col
          xs={0}
          sm={0}
          md={0}
          lg={16}
          xl={16}
          style={{ position: "relative" }}
        >
          <div
            style={{
              position: "absolute",
              right: 0,
              background: `url(${loginLeft})`,
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
              backgroundPosition: "100%",
              height: "100vh",
              width: "100%",
              color: "#fff",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
              padding: 50,
            }}
          >
            <div
              style={{
                position: "absolute",
                top: 10,
                left: 20,
                zIndex: 999,
                display: "flex",
                alignItems: "center",
              }}
            >
              <img
                src={logo}
                width={50}
                alt="Logo"
                style={{ marginRight: 10 }}
              />
              <span style={{ fontSize: 17, fontWeight: "bold" }}>
                Smkn 1 Katapang
              </span>
            </div>
            <div
              style={{
                maxWidth: "30vw",
                textAlign: "center",
                backdropFilter: "blur(10px)",
                padding: "50px 50px 50px 50px",
                WebkitBackdropFilter: "blur(10px)",
                backgroundColor: "rgba(255, 255, 255, 0.5)",
                borderRadius: 10,
              }}
            >
              <Carousel autoplay>
                <div>
                  <ReconciliationOutlined
                    style={{
                      fontSize: 60,
                      color: "var(--ant-primary-color)",
                      marginBottom: 10,
                    }}
                  />
                  <div
                    style={{
                      fontSize: 15,
                      fontWeight: "bold",
                      color: "#294354",
                    }}
                  >
                    E-Inventory
                  </div>
                  <div style={{ fontSize: 12, opacity: 0.9, color: "#294354" }}>
                    Digitalisasi pengelolaan alat bengkel setiap jurusan yang
                    ada di SMKN 1 Katapang
                  </div>
                </div>
              </Carousel>
            </div>
          </div>
          <div
            style={{
              textAlign: "center",
              color: "#fff",
              position: "absolute",
              bottom: 20,
              width: "100%",
              fontSize: 12,
            }}
          >
            Copyright &copy;{new Date().getFullYear()} RPL SMKN 1 Katapang | All
            Right Reserved
          </div>
        </Col>
        <Col
          xs={24}
          sm={24}
          md={24}
          lg={8}
          xl={8}
          style={{
            height: "100%",
          }}
        >
          <Card
            bordered={false}
            style={{
              height: "100%",
              overflowY: "auto",
              padding: 20,
            }}
            className="middle"
          >
            <div className="flex flex-column">
              <span
                style={{
                  fontSize: 15,
                  marginTop: 15,
                  color: "var(--ant-primary-color)",
                  textTransform: "uppercase",
                  fontWeight: "bold",
                }}
              >
                {title}
              </span>
            </div>
            <div style={{ marginTop: 10 }}>{children}</div>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
export default FullLayout;
