import logo from "../assets/img/logo.png";

function PrintLayout(props) {
  const { children, title, subtitle } = props;
  return (
    <>
      <div style={{ display: "flex", alignItems: "center", marginBottom: 20 }}>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div
            style={{
              fontWeight: "bold",
              borderBottom: "3px solid #000",
              letterSpacing: "5px",
            }}
          >
            Smkn 1 Katapang
          </div>
          <div
            style={{ letterSpacing: "5px", fontSize: 30, fontWeight: "bold" }}
          >
            {title}
          </div>
          <div>{subtitle}</div>
        </div>
        <div style={{ marginLeft: "auto" }}>
          <img
            src={logo}
            style={{ width: "150px", marginRight: 10 }}
            alt="Logo"
          />
        </div>
      </div>
      {children}
    </>
  );
}
export default PrintLayout;
