import Card from "antd/es/card";
import React from "react";
import MainLayout from "../layouts/MainLayout";
function DashboardPage() {
  return (
    <MainLayout title="Dashboard">
      <Card title="Welcome Admin!">Have an nice day:)</Card>
    </MainLayout>
  );
}
export default DashboardPage;
