import TableLocation from "../components/Tables/TableLocation";
import MainLayout from "../layouts/MainLayout";

function LocationPage() {
  return (
    <MainLayout title="Lokasi">
      <TableLocation />
    </MainLayout>
  );
}
export default LocationPage;
