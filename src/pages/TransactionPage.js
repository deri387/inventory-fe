import TableTransaction from "../components/Tables/TableTransaction";
import MainLayout from "../layouts/MainLayout";

function TransactionPage() {
  return (
    <MainLayout title={"Transaksi"}>
      <TableTransaction />
    </MainLayout>
  );
}
export default TransactionPage;
