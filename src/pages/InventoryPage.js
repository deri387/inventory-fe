import TableInventory from "../components/Tables/TableInventory";
import MainLayout from "../layouts/MainLayout";

function InventoryPage() {
  return (
    <MainLayout title="Peralatan">
      <TableInventory />
    </MainLayout>
  );
}
export default InventoryPage;
