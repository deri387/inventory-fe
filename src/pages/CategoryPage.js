import TableCategory from "../components/Tables/TableCategory";
import MainLayout from "../layouts/MainLayout";

function CategoryPage() {
  return (
    <MainLayout title="Kategori">
      <TableCategory />
    </MainLayout>
  );
}
export default CategoryPage;
