import LoginForm from "../components/Forms/LoginForm";
import FullLayout from "../layouts/FullLayout";

function LoginPage() {
  return (
    <FullLayout title="Login">
      <div style={{ fontSize: 12, marginBottom: "2vh" }}>
        Silakan gunakan kredensial Anda untuk login. Jika Anda tidak memiliki
        akun, silakan hubungi admin untuk membuat akun Anda.
      </div>
      <LoginForm />
    </FullLayout>
  );
}
export default LoginPage;
