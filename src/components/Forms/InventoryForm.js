import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import Divider from "antd/es/divider";
import Select from "antd/es/select";
import { PlusOutlined } from "@ant-design/icons";
import _ from "lodash";
import React, { useEffect } from "react";

import http from "../../utils/http";

function InventoryForm(props) {
  const [form] = Form.useForm();
  const [categories, setCategories] = React.useState([]);
  const [category, setCategory] = React.useState("");
  const [locations, setLocations] = React.useState([]);
  const [location, setLocation] = React.useState("");

  const onFinish = (values) => {
    if (props.data) {
      http.put("/v1/inventory", values).then(async (res) => {
        if (res) {
          form.resetFields();
          props.handleClose();
        }
      });
    } else {
      http.post("/v1/inventory", values).then(async (res) => {
        if (res) {
          form.resetFields();
          props.fetch();
          //props.handleClose();
        }
      });
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const init = {
    id: props.data ? props.data.id : "",
    name: props.data ? props.data.name : "",
    inventory_location_id: props.data ? props.data.inventory_location_id : "",
    inventory_category_id: props.data ? props.data.inventory_category_id : "",
    qty: props.data ? props.data.qty : "",
    condition: props.data ? props.data.condition : "",
    condition_description: props.data ? props.data.condition_description : "",
  };
  useEffect(() => {
    form.setFieldsValue(init);
    fetchCategory("");
    fetchLocation("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.data]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchSearhcCategory = React.useCallback(
    _.debounce((e) => {
      fetchCategory(e);
    }, 1000)
  );

  const fetchCategory = (data) => {
    http.get(`/v1/category?name=${data}&page=1`).then((res) => {
      if (res) {
        setCategories(res.data.serve.data);
      }
    });
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchSearchLocation = React.useCallback(
    _.debounce((e) => {
      fetchLocation(e);
    }, 1000)
  );

  const fetchLocation = (data) => {
    http.get(`/v1/location?name=${data}&page=1`).then((res) => {
      if (res) {
        setLocations(res.data.serve.data);
      }
    });
  };

  return (
    <Form
      form={form}
      name="basic"
      layout="vertical"
      initialValues={init}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item label="ID" name="id" hasFeedback hidden>
        <Input type="hidden" />
      </Form.Item>
      <Form.Item
        label="Nama"
        name="name"
        rules={[{ required: true, message: "Nama wajib diisi" }]}
        hasFeedback
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Kategori"
        name="inventory_category_id"
        rules={[{ required: true, message: "Kategori wajib diisi" }]}
        hasFeedback
      >
        <Select
          showSearch
          allowClear
          defaultActiveFirstOption={false}
          filterOption={false}
          onSearch={(e) => fetchSearhcCategory(e)}
          dropdownRender={(menu) => (
            <div>
              {menu}
              <Divider style={{ margin: "4px 0" }} />
              <div
                style={{
                  display: "flex",
                  flexWrap: "nowrap",
                  padding: 8,
                }}
              >
                <Input
                  style={{ flex: "auto" }}
                  value={category}
                  placeholder="Buat kategori baru..."
                  onChange={(e) => setCategory(e.target.value)}
                />
                <Button
                  icon={<PlusOutlined />}
                  type="link"
                  onClick={() => {
                    http
                      .post("/v1/category", {
                        name: category,
                      })
                      .then(async (res) => {
                        if (res) {
                          await fetchCategory("");
                          await setCategory("");
                        }
                      });
                  }}
                >
                  Buat baru
                </Button>
              </div>
            </div>
          )}
        >
          {categories.length > 0
            ? categories.map((v, i) => (
                <Select.Option value={v.id} key={i}>
                  {v.name}
                </Select.Option>
              ))
            : null}
        </Select>
      </Form.Item>
      <Form.Item
        label="Lokasi"
        name="inventory_location_id"
        rules={[{ required: true, message: "Lokasi wajib diisi" }]}
        hasFeedback
      >
        <Select
          showSearch
          allowClear
          defaultActiveFirstOption={false}
          filterOption={false}
          onSearch={(e) => fetchSearchLocation(e)}
          dropdownRender={(menu) => (
            <div>
              {menu}
              <Divider style={{ margin: "4px 0" }} />
              <div
                style={{
                  display: "flex",
                  flexWrap: "nowrap",
                  padding: 8,
                }}
              >
                <Input
                  style={{ flex: "auto" }}
                  value={location}
                  placeholder="Buat lokasi baru..."
                  onChange={(e) => setLocation(e.target.value)}
                />
                <Button
                  icon={<PlusOutlined />}
                  type="link"
                  onClick={() => {
                    http
                      .post("/v1/location", {
                        name: location,
                      })
                      .then(async (res) => {
                        if (res) {
                          await fetchLocation("");
                          await setLocation("");
                        }
                      });
                  }}
                >
                  Buat baru
                </Button>
              </div>
            </div>
          )}
        >
          {locations.length > 0
            ? locations.map((v, i) => (
                <Select.Option value={v.id} key={i}>
                  {v.name}
                </Select.Option>
              ))
            : null}
        </Select>
      </Form.Item>
      <Form.Item
        label="Qty"
        name="qty"
        rules={[{ required: true, message: "Qty wajib diisi" }]}
        hasFeedback
      >
        <Input type="number" />
      </Form.Item>
      <Form.Item
        label="Kondisi"
        name="condition"
        rules={[{ required: true, message: "Kondisi wajib diisi" }]}
        hasFeedback
      >
        <Select>
          <Select.Option value={"BAIK"}>BAIK</Select.Option>
          <Select.Option value={"TIDAK BAIK"}>TIDAK BAIK</Select.Option>
        </Select>
      </Form.Item>
      <Form.Item label="Keterangan" name="condition_description">
        <Input.TextArea />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" block shape="round">
          Simpan
        </Button>
      </Form.Item>
    </Form>
  );
}
export default InventoryForm;
