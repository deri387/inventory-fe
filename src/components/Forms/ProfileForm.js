import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import http from "../../utils/http";
import helper from "../../utils/helper";
function ProfileForm(props) {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    const storage = helper.isAuthenticated();
    values.id = storage.user.id;
    http.put("/v1/user", values).then(async (res) => {
      if (res) {
        storage.user.name = res.data.serve.name;
        localStorage.setItem("db", JSON.stringify(storage));
        window.location.reload(true);
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      form={form}
      name="basic"
      layout="vertical"
      initialValues={{
        name: helper.isAuthenticated()?.user?.name,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Nama"
        name="name"
        rules={[{ required: true, message: "Nama wajib diisi" }]}
        hasFeedback
      >
        <Input />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" block shape="round">
          Update
        </Button>
      </Form.Item>
    </Form>
  );
}
export default ProfileForm;
