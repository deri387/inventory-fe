import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import React, { useEffect } from "react";
import http from "../../utils/http";

function CategoryForm(props) {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    if (props.data) {
      http.put("/v1/category", values).then(async (res) => {
        if (res) {
          form.resetFields();
          props.handleClose();
        }
      });
    } else {
      http.post("/v1/category", values).then(async (res) => {
        if (res) {
          form.resetFields();
          props.fetch();
          //props.handleClose();
        }
      });
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const init = {
    id: props.data ? props.data.id : "",
    name: props.data ? props.data.name : "",
  };
  useEffect(() => {
    form.setFieldsValue(init);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.data]);

  return (
    <Form
      form={form}
      name="basic"
      layout="vertical"
      initialValues={init}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item label="ID" name="id" hasFeedback hidden>
        <Input type="hidden" />
      </Form.Item>
      <Form.Item
        label="Kategori"
        name="name"
        rules={[{ required: true, message: "Kategori wajib diisi" }]}
        hasFeedback
      >
        <Input />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" block shape="round">
          Simpan
        </Button>
      </Form.Item>
    </Form>
  );
}
export default CategoryForm;
