import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import { LockOutlined } from "@ant-design/icons";
import http from "../../utils/http";
import helper from "../../utils/helper";
function ChangePassword(props) {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    values.id = helper.isAuthenticated().user.id;
    http.put("/v1/user/change-password", values).then(async (res) => {
      if (res) {
        await form.resetFields();
        props.handleClose();
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      form={form}
      name="basic"
      layout="vertical"
      initialValues={{
        old_password: "",
        password: "",
        password_confirmation: "",
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Password lama"
        name="old_password"
        rules={[{ required: true, message: "Password lama wajib diisi" }]}
        hasFeedback
      >
        <Input.Password
          prefix={<LockOutlined />}
          placeholder="Masukan password lama"
        />
      </Form.Item>
      <Form.Item
        label="Password baru"
        name="password"
        rules={[
          { required: true, message: "Password wajib diisi" },
          { min: 8, message: "Password minimal terdiri dari 8 karakter" },
        ]}
        hasFeedback
      >
        <Input.Password
          prefix={<LockOutlined />}
          placeholder="Buat password baru"
        />
      </Form.Item>
      <Form.Item
        label="Konfirmasi password"
        name="password_confirmation"
        rules={[
          {
            required: true,
            message: "Konfirmasi password wajib diisi",
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error("Password tidak sama"));
            },
          }),
        ]}
        hasFeedback
      >
        <Input.Password
          prefix={<LockOutlined />}
          placeholder="Ketik password kembali"
        />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" block shape="round">
          Update
        </Button>
      </Form.Item>
    </Form>
  );
}
export default ChangePassword;
