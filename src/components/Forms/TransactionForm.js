import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import Select from "antd/es/select";
import Divider from "antd/es/divider";
import Row from "antd/es/row";
import Col from "antd/es/col";
import React from "react";
import * as _ from "lodash";
import { PlusOutlined, CloseOutlined } from "@ant-design/icons";
import http from "../../utils/http";

function TransactionForm(props) {
  const [form] = Form.useForm();
  const [products, setProducts] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const [details, setDetails] = React.useState([
    {
      inventory_id: "",
      qty: "",
      status: 1,
    },
  ]);

  const onFinish = (values) => {
    if (open) {
      values.products = details;
      http.put("/v1/transaction", values).then(async (res) => {
        if (res) {
          setOpen(false);
          form.resetFields();
          props.handleClose();
        }
      });
    } else {
      http.post("/v1/transaction", values).then(async (res) => {
        if (res) {
          setOpen(true);
        }
      });
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  React.useEffect(() => {
    fetchProduct("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchSearchProduct = React.useCallback(
    _.debounce((e) => {
      fetchProduct(e);
    }, 1000)
  );

  const fetchProduct = (data) => {
    http.get(`/v1/inventory?name=${data}&page=1`).then((res) => {
      if (res) {
        setProducts(res.data.serve.data);
      }
    });
  };

  const randomNumber = (min, max) => {
    return Math.random() * (max - min) + min;
  };
  return (
    <Form
      form={form}
      name="basic"
      layout="vertical"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      style={{ width: "100%" }}
    >
      <Form.Item
        style={{ marginBottom: 5 }}
        label="Kode pinjam"
        name="transaction_number"
        rules={[{ required: true, message: "Kode wajib diisi" }]}
        hasFeedback
      >
        <Input readOnly={open} />
      </Form.Item>
      <Form.Item>
        <Button
          type="primary"
          size="small"
          onClick={() => {
            const dirtyForm = form;
            dirtyForm.transaction_number = parseInt(
              randomNumber(0, 1000000000)
            );
            form.setFieldsValue(dirtyForm);
          }}
        >
          Random
        </Button>
      </Form.Item>

      <Form.Item
        label="Nama peminjam"
        name="name"
        rules={[{ required: true, message: "Nama wajib diisi" }]}
        hasFeedback
      >
        <Input readOnly={open} />
      </Form.Item>

      <Form.Item label="No kontak peminjam" name="contact">
        <Input readOnly={open} />
      </Form.Item>

      <Row gutter={[12, 12]} align="middle">
        {open ? (
          <>
            {details.map((v, i) => (
              <React.Fragment key={i}>
                <Col xs={24} sm={24} md={24} lg={10} xl={10}>
                  <Form.Item label="Alat">
                    <Select
                      showSearch
                      allowClear
                      defaultActiveFirstOption={false}
                      showArrow
                      filterOption={false}
                      onSearch={(e) => fetchSearchProduct(e)}
                      onChange={(e) => {
                        details[i].inventory_id = e;
                        setDetails([...details]);
                      }}
                      value={v.inventory_id ? v.inventory_id : null}
                    >
                      {products.length > 0
                        ? products.map((v, i) => (
                            <Select.Option value={v.id} key={i}>
                              {v.name}
                            </Select.Option>
                          ))
                        : null}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={24} lg={11} xl={11}>
                  <Form.Item label="Qty">
                    <Input
                      type={"number"}
                      value={v.qty}
                      onChange={(e) => {
                        details[i].qty = e.target.value;
                        setDetails([...details]);
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={24} lg={3} xl={3}>
                  {i > 0 ? (
                    <>
                      <Button
                        shape="circle"
                        icon={<PlusOutlined />}
                        onClick={() => {
                          setDetails([
                            ...details,
                            {
                              inventory_id: "",

                              qty: "",
                            },
                          ]);
                        }}
                      ></Button>

                      <Button
                        type="danger"
                        shape="circle"
                        style={{ marginLeft: 10 }}
                        icon={<CloseOutlined />}
                        onClick={() => {
                          details.splice(i, 1);
                          setDetails([...details]);
                        }}
                      ></Button>
                    </>
                  ) : (
                    <Button
                      shape="circle"
                      icon={<PlusOutlined />}
                      onClick={() => {
                        setDetails([
                          ...details,
                          {
                            inventory_id: "",

                            qty: "",
                          },
                        ]);
                      }}
                    ></Button>
                  )}
                </Col>
                <Divider />
              </React.Fragment>
            ))}
          </>
        ) : null}

        <Col xs={24} sm={24} md={24} lg={24}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ float: "right", marginLeft: 10 }}
            shape="round"
          >
            Simpan
          </Button>
          <Button
            onClick={() => {
              if (form.getFieldValue("transaction_number")) {
                http({
                  url: "/v1/transaction/number",
                  method: "DELETE",
                  data: {
                    transaction_number: form.getFieldValue(
                      "transaction_number"
                    ),
                  },
                })
                  .then(() => {
                    props.handleClose();
                  })
                  .catch(() => {
                    props.handleClose();
                  });
              } else {
                props.handleClose();
              }
            }}
            style={{ float: "right" }}
            shape="round"
          >
            Batal
          </Button>
        </Col>
      </Row>
    </Form>
  );
}
export default TransactionForm;
