import { Chart } from "@antv/g2";
import { useEffect, useState } from "react";

function ChartParentBar(props) {
  const [chart, setChart] = useState(null);
  useEffect(() => {
    setChart(
      new Chart({
        container: "parent-bar",
        autoFit: true,
        height: 300,
      })
    );
  }, []);

  useEffect(() => {
    if (props.data && chart) {
      chart.data(props.data);

      chart.scale("value", {
        alias: "Total Wali",
      });

      chart.axis("time", {
        tickLine: null,
      });

      chart.axis("value", {
        label: {
          formatter: (text) => {
            return text.replace(/(\d)(?=(?:\d{3})+$)/g, "$1,");
          },
        },
        title: {
          offset: 80,
          style: {
            fill: "#aaaaaa",
          },
        },
      });
      chart.legend({
        position: "bottom",
      });

      chart.tooltip({
        shared: true,
        showMarkers: false,
      });
      chart.interaction("active-region");

      chart
        .interval()
        .adjust("stack")
        .position("time*value")
        .color("time", ["#ff4d4f"]);

      chart.render();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.data, chart]);
  return <div id="parent-bar"></div>;
}
export default ChartParentBar;
