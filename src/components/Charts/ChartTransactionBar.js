import { Chart } from "@antv/g2";
import { useEffect, useState } from "react";

function ChartTransactionBar(props) {
  const [chart, setChart] = useState(null);
  useEffect(() => {
    setChart(
      new Chart({
        container: "container",
        autoFit: true,
        height: 300,
      })
    );
  }, []);

  useEffect(() => {
    if (props.data && chart) {
      chart.data(props.data);

      chart.scale("value", {
        alias: "Total Transaksi",
      });

      chart.axis("month", {
        tickLine: null,
      });

      chart.axis("value", {
        label: {
          formatter: (text) => {
            return text.replace(/(\d)(?=(?:\d{3})+$)/g, "$1,");
          },
        },
        title: {
          offset: 80,
          style: {
            fill: "#aaaaaa",
          },
        },
      });
      chart.legend({
        position: "bottom",
      });

      chart.tooltip({
        shared: true,
        showMarkers: false,
      });
      chart.interaction("active-region");

      chart
        .interval()
        .adjust("stack")
        .position("month*value")
        .color("type", ["#ff4d4f", "#08979c", "#52c41a", "#faad14", "gold"]);

      chart.render();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.data, chart]);
  return <div id="container"></div>;
}
export default ChartTransactionBar;
