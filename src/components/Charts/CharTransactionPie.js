import { Chart } from "@antv/g2";
import { useEffect, useState } from "react";

function ChartTransactionPie(props) {
  const [chart, setChart] = useState(null);
  useEffect(() => {
    setChart(
      new Chart({
        container: "transaction-chart",
        autoFit: true,
        height: 300,
      })
    );
  }, []);

  useEffect(() => {
    if (props.data && chart) {
      chart.data(props.data);

      chart.coordinate("theta", {
        radius: 0.85,
      });

      chart.scale("percent", {
        formatter: (val) => {
          val = val + "%";
          return val;
        },
      });
      chart.tooltip({
        showTitle: false,
        showMarkers: false,
      });
      chart.axis(false);
      const interval = chart
        .interval()
        .adjust("stack")
        .position("percent")
        .color("item", ["#ff4d4f", "#08979c", "#52c41a", "#faad14", "gold"])
        .label("percent", {
          offset: -40,
          style: {
            textAlign: "center",
            shadowBlur: 2,
            shadowColor: "rgba(0, 0, 0, .45)",
            fill: "#fff",
          },
        })
        .tooltip("item*count", (item, percent) => {
          return {
            name: item,
            value: percent,
          };
        })
        .style({
          lineWidth: 1,
          stroke: "#fff",
        });
      chart.interaction("element-single-selected");
      chart.render();

      // 默认选择
      interval.elements[0].setState("selected", true);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.data, chart]);
  return <div id="transaction-chart"></div>;
}
export default ChartTransactionPie;
