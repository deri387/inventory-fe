import React from "react";
import Table from "antd/es/table";
import Button from "antd/es/button";
import Input from "antd/es/input";
import Card from "antd/es/card";
import Dropdown from "antd/es/dropdown";
import Menu from "antd/es/menu";
import Popconfirm from "antd/es/popconfirm";
import Select from "antd/es/select";
import Drawer from "antd/es/drawer";
import Tag from "antd/es/tag";
import Row from "antd/es/row";
import Col from "antd/es/col";
import Spin from "antd/es/spin";
import _ from "lodash";
import {
  PlusOutlined,
  DeleteOutlined,
  EditOutlined,
  MoreOutlined,
  SearchOutlined,
  PrinterOutlined,
} from "@ant-design/icons";
import ReactToPrint, { PrintContextConsumer } from "react-to-print";
import http from "../../utils/http";
import helper from "../../utils/helper";
import InventoryForm from "../Forms/InventoryForm";
import PrintLayout from "../../layouts/PrintLayout";

const pageStyle = `
@media all {
  .page-break {
    display: none;
  }
}

@media print {
  html, body {
    height: initial !important;
    overflow: initial !important;
    -webkit-print-color-adjust: exact;
  }
}

@media print {
  .page-break {
    margin-top: 1rem;
    display: block;
    page-break-before: auto;
  }
}

@page {
  size: auto;
  margin: 10mm;
}
`;
const columns = (props) => [
  {
    title: "Nama",
    dataIndex: "name",
  },
  {
    title: "Kategory",
    dataIndex: "category",
    render: (text, record, index) => record?.category?.name,
    responsive: ["lg"],
  },
  {
    title: "Lokasi",
    dataIndex: "location",
    render: (text, record, index) => record?.location?.name,
    responsive: ["lg"],
  },
  {
    title: "Qty",
    dataIndex: "qty",
    responsive: ["lg"],
  },
  {
    title: "Kondisi",
    dataIndex: "condition",
    render: (text, record, index) =>
      record?.condition === "BAIK" ? (
        <Tag color={"green"}>BAIK</Tag>
      ) : (
        <Tag color={"red"}>TIDAK BAIK</Tag>
      ),
    responsive: ["lg"],
  },
  {
    title: "Keterangan",
    dataIndex: "condition_description",
    responsive: ["lg"],
  },
  {
    title: "#",
    width: "10%",
    align: "center",
    dataIndex: "action",
    render: (text, record) => (
      <Dropdown
        arrow
        overlayStyle={{ width: "150px" }}
        overlay={
          <Menu>
            <Menu.Item
              key="/edit"
              icon={<EditOutlined />}
              onClick={() => {
                props.setCurrent(record);
                props.setOpen(true);
              }}
            >
              Edit
            </Menu.Item>
            <Popconfirm
              placement="left"
              title="Anda yakin ingin menghapus data ini?"
              onConfirm={() => {
                const postData = { id: record.id };
                http({
                  url: "/v1/inventory",
                  method: "DELETE",
                  data: postData,
                }).then(() => {
                  props.setCurrent(false);
                  props.fetch("");
                });
              }}
              okText="Iya"
              cancelText="Batal"
            >
              <Menu.Item key="/delete" icon={<DeleteOutlined />}>
                Hapus
              </Menu.Item>
            </Popconfirm>
          </Menu>
        }
      >
        <a
          href="/#"
          className="ant-dropdown-link"
          onClick={(e) => e.preventDefault()}
        >
          <MoreOutlined
            style={{ color: "#000", fontWeight: "bold", fontSize: 20 }}
          />
        </a>
      </Dropdown>
    ),
  },
];
const columnsPrint = (props) => [
  {
    title: "Nama",
    dataIndex: "name",
  },
  {
    title: "Kategory",
    dataIndex: "category",
    render: (text, record, index) => record?.category?.name,
    responsive: ["lg"],
  },
  {
    title: "Lokasi",
    dataIndex: "location",
    render: (text, record, index) => record?.location?.name,
    responsive: ["lg"],
  },
  {
    title: "Qty",
    dataIndex: "qty",
    responsive: ["lg"],
  },
  {
    title: "Kondisi",
    dataIndex: "condition",
    render: (text, record, index) =>
      record?.condition === "BAIK" ? (
        <Tag color={"green"}>BAIK</Tag>
      ) : (
        <Tag color={"red"}>TIDAK BAIK</Tag>
      ),
    responsive: ["lg"],
  },
  {
    title: "Keterangan",
    dataIndex: "condition_description",
    responsive: ["lg"],
  },
];

function TableInventory(props) {
  const printComponent = React.useRef();
  const [data, setData] = React.useState([]);
  const [categories, setCategories] = React.useState([]);
  const [locations, setLocations] = React.useState([]);
  const [params, setParams] = React.useState({
    name: "",
    category: "",
    location: "",
    condition: "",
  });
  const [pagination, setPagination] = React.useState({
    current: 1,
    pageSize: 10,
    total: 0,
  });
  const [open, setOpen] = React.useState(false);
  const [current, setCurrent] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [isPrint, setIsPrint] = React.useState(false);
  React.useEffect(() => {
    fetch(params, pagination);
    fetchCategory("");
    fetchLocation("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleTableChange = async (page) => {
    fetch(params, page);
  };

  const fetch = (data = params, pagination) => {
    setLoading(true);
    http
      .get(
        `/v1/inventory?name=${data.name}&category=${data.category}&location=${data.location}&condition=${data.condition}&page=${pagination?.current}&pageSize=${pagination?.pageSize}`
      )
      .then(async (data) => {
        await setLoading(false);
        await setData(data?.data?.serve.data);
        await setPagination({
          current: data?.data?.serve.current_page,
          pageSize: data?.data?.serve.per_page,
          total: data?.data?.serve.total,
        });
      });
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchSearhcCategory = React.useCallback(
    _.debounce((e) => {
      fetchCategory(e);
    }, 1000)
  );

  const fetchCategory = (data) => {
    http.get(`/v1/category?name=${data}&page=1`).then((res) => {
      if (res) {
        setCategories(res.data.serve.data);
      }
    });
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchSearchLocation = React.useCallback(
    _.debounce((e) => {
      fetchLocation(e);
    }, 1000)
  );

  const fetchLocation = (data) => {
    http.get(`/v1/location?name=${data}&page=1`).then((res) => {
      if (res) {
        setLocations(res.data.serve.data);
      }
    });
  };

  return (
    <Spin
      spinning={isPrint}
      tip="Memproses data, mohon tunggu.."
      className="h-full"
    >
      <style>{pageStyle}</style>
      <Card className="card-product-filter" style={{ marginTop: 10 }}>
        <Row
          gutter={[12, 12]}
          style={{ width: "100%" }}
          justify="center"
          align="middle"
        >
          <Col xs={24} sm={24} md={24} lg={6} xl={6}>
            <Input
              type="text"
              onChange={(e) => {
                setParams({
                  name: e.target.value,
                  category: params.category,
                  location: params.location,
                  condition: params.condition,
                });
              }}
              placeholder="Filter berdasar nama"
            />
          </Col>
          <Col xs={24} sm={24} md={24} lg={5} xl={5}>
            <Select
              placeholder="Filter berdasar kategori"
              showSearch
              allowClear
              style={{ width: "100%" }}
              defaultActiveFirstOption={false}
              filterOption={false}
              onSearch={(e) => fetchSearhcCategory(e)}
              onChange={(e) => {
                setParams({
                  name: params.name,
                  category: e,
                  location: params.location,
                  condition: params.condition,
                });
              }}
              notFoundContent={null}
              value={params.category ? params.category : null}
            >
              {categories.length > 0
                ? categories.map((v, i) => (
                    <Select.Option value={v.id} key={i}>
                      {v.name}
                    </Select.Option>
                  ))
                : null}
            </Select>
          </Col>
          <Col xs={24} sm={24} md={24} lg={5} xl={5}>
            <Select
              style={{ width: "100%" }}
              showSearch
              allowClear
              defaultActiveFirstOption={false}
              filterOption={false}
              onSearch={(e) => fetchSearchLocation(e)}
              onChange={(e) => {
                setParams({
                  name: params.name,
                  category: params.category,
                  location: e,
                  condition: params.condition,
                });
              }}
              notFoundContent={null}
              value={params.location ? params.location : null}
              placeholder="Filter berdasar lokasi"
            >
              {locations.length > 0
                ? locations.map((v, i) => (
                    <Select.Option value={v.id} key={i}>
                      {v.name}
                    </Select.Option>
                  ))
                : null}
            </Select>
          </Col>
          <Col xs={24} sm={24} md={24} lg={5} xl={5}>
            <Select
              style={{ width: "100%" }}
              onChange={(e) => {
                setParams({
                  name: params.name,
                  category: params.category,
                  location: params.location,
                  condition: e,
                });
              }}
              value={params.condition ? params.condition : null}
              placeholder="Filter berdasar kondisi"
            >
              <Select.Option value={"BAIK"}>BAIK</Select.Option>
              <Select.Option value={"TIDAK BAIK"}>TIDAK BAIK</Select.Option>
            </Select>
          </Col>
          <Col xs={24} sm={24} md={24} lg={2} xl={2}>
            <Button
              icon={<SearchOutlined />}
              type="primary"
              onClick={() => {
                fetch(params, pagination);
              }}
              style={{ width: "100%" }}
            >
              Cari
            </Button>
          </Col>
          <Col xs={24} sm={24} md={24} lg={1} xl={1}>
            <Button
              type="link"
              onClick={() => {
                setParams({
                  name: "",
                  category: "",
                  location: "",
                  condition: "",
                });
                fetch(
                  {
                    name: "",
                    category: "",
                    location: "",
                    condition: "",
                  },
                  pagination
                );
              }}
            >
              Reset
            </Button>
          </Col>
        </Row>
      </Card>
      <Card className="card-product-filter" style={{ marginTop: 10 }}>
        <div
          className="flex flex-wrap"
          style={{ width: "100%", alignItems: "flex-end" }}
        >
          <div className="flex align-center">
            <span style={{ fontSize: 12 }}>Show</span>
            <Select
              defaultActiveFirstOption={false}
              onChange={(e) => {
                setPagination({
                  current: pagination.current,
                  pageSize: e,
                  total: pagination.total,
                });
                fetch(params, {
                  current: pagination.current,
                  pageSize: e,
                  total: pagination.total,
                });
              }}
              style={{ width: "60px", marginLeft: 10, marginRight: 10 }}
              value={pagination.pageSize}
            >
              <Select.Option value="10">10</Select.Option>
              <Select.Option value="50">50</Select.Option>
              <Select.Option value="100">100</Select.Option>
              <Select.Option value="500">500</Select.Option>
            </Select>
            <span style={{ fontSize: 12 }}>entries</span>
          </div>
          <div style={{ marginLeft: "auto" }} className="flex align-center">
            <ReactToPrint
              content={() => printComponent.current}
              onAfterPrint={() => {
                setIsPrint(false);
              }}
            >
              <PrintContextConsumer>
                {({ handlePrint }) => (
                  <Button
                    icon={<PrinterOutlined />}
                    style={{
                      backgroundColor: "green",
                      marginRight: 10,
                    }}
                    type="primary"
                    onClick={async () => {
                      await setIsPrint(true);
                      handlePrint();
                    }}
                  >
                    Print
                  </Button>
                )}
              </PrintContextConsumer>
            </ReactToPrint>
            <Button
              icon={<PlusOutlined />}
              type="primary"
              onClick={() => setOpen(true)}
              style={{ marginRight: 10 }}
            >
              Buat baru
            </Button>
          </div>
        </div>
      </Card>
      {isPrint ? (
        <div ref={printComponent}>
          <PrintLayout
            title={`PERALATAN BENGKEL`}
            subtitle={`Jurusan ${
              helper.isAuthenticated()?.user?.subject?.name
            }`}
          >
            <Table
              ref={printComponent}
              columns={columnsPrint()}
              rowKey={(record) => record.id}
              dataSource={data}
              pagination={false}
              loading={loading}
              onChange={handleTableChange}
            />
          </PrintLayout>
        </div>
      ) : null}
      <Table
        style={{ marginTop: 10 }}
        columns={columns({
          fetch: () => fetch(params, pagination),
          setOpen: (data) => setOpen(data),
          setCurrent: (data) => setCurrent(data),
        })}
        rowKey={(record) => record.id}
        dataSource={data}
        pagination={pagination}
        loading={loading}
        onChange={handleTableChange}
      />
      <Drawer
        title="Kelola Peralatan"
        onClose={() => {
          setOpen(false);
          setCurrent(false);
          fetch(params, pagination);
        }}
        visible={open}
      >
        <InventoryForm
          data={current}
          handleClose={() => {
            setOpen(false);
            setCurrent(false);
            fetch(params, pagination);
          }}
          fetch={() => fetch(params, pagination)}
        />
      </Drawer>
    </Spin>
  );
}

export default TableInventory;
