import React from "react";
import Table from "antd/es/table";
import Button from "antd/es/button";
import Input from "antd/es/input";
import Card from "antd/es/card";
import Dropdown from "antd/es/dropdown";
import Menu from "antd/es/menu";
import Popconfirm from "antd/es/popconfirm";
import Select from "antd/es/select";
import Drawer from "antd/es/drawer";
import {
  PlusOutlined,
  DeleteOutlined,
  EditOutlined,
  MoreOutlined,
} from "@ant-design/icons";
import http from "../../utils/http";
import CategoryForm from "../Forms/CategoryForm";

const columns = (props) => [
  {
    title: "Kategori",
    dataIndex: "name",
  },
  {
    title: "#",
    width: "10%",
    align: "center",
    dataIndex: "action",
    render: (text, record) => (
      <Dropdown
        arrow
        overlayStyle={{ width: "150px" }}
        overlay={
          <Menu>
            <Menu.Item
              key="/edit"
              icon={<EditOutlined />}
              onClick={() => {
                props.setCurrent(record);
                props.setOpen(true);
              }}
            >
              Edit
            </Menu.Item>
            <Popconfirm
              placement="left"
              title="Anda yakin ingin menghapus data ini?"
              onConfirm={() => {
                const postData = { id: record.id };
                http({
                  url: "/v1/category",
                  method: "DELETE",
                  data: postData,
                }).then(() => {
                  props.setCurrent(false);
                  props.fetch("");
                });
              }}
              okText="Iya"
              cancelText="Batal"
            >
              <Menu.Item key="/delete" icon={<DeleteOutlined />}>
                Hapus
              </Menu.Item>
            </Popconfirm>
          </Menu>
        }
      >
        <a
          href="/#"
          className="ant-dropdown-link"
          onClick={(e) => e.preventDefault()}
        >
          <MoreOutlined
            style={{ color: "#000", fontWeight: "bold", fontSize: 20 }}
          />
        </a>
      </Dropdown>
    ),
  },
];

function TableCategory() {
  const [data, setData] = React.useState([]);
  const [params, setParams] = React.useState({
    name: "",
  });
  const [pagination, setPagination] = React.useState({
    current: 1,
    pageSize: 10,
    total: 0,
  });
  const [open, setOpen] = React.useState(false);
  const [current, setCurrent] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const { Search } = Input;

  React.useEffect(() => {
    fetch(params, pagination);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleTableChange = async (page) => {
    fetch(params, page);
  };

  const fetch = (data = params, pagination) => {
    setLoading(true);
    http
      .get(
        `/v1/category?name=${data.name}&page=${pagination?.current}&pageSize=${pagination?.pageSize}`
      )
      .then(async (data) => {
        await setLoading(false);
        await setData(data?.data?.serve.data);
        await setPagination({
          current: data?.data?.serve.current_page,
          pageSize: data?.data?.serve.per_page,
          total: data?.data?.serve.total,
        });
      });
  };

  return (
    <>
      <Card className="card-product-filter" style={{ marginTop: 10 }}>
        <div
          className="flex flex-wrap"
          style={{ width: "100%", alignItems: "flex-end" }}
        >
          <div className="flex align-center">
            <span style={{ fontSize: 12 }}>Show</span>
            <Select
              defaultActiveFirstOption={false}
              onChange={(e) => {
                setPagination({
                  current: pagination.current,
                  pageSize: e,
                  total: pagination.total,
                });
                fetch(params, {
                  current: pagination.current,
                  pageSize: e,
                  total: pagination.total,
                });
              }}
              style={{ width: "60px", marginLeft: 10, marginRight: 10 }}
              value={pagination.pageSize}
            >
              <Select.Option value="10">10</Select.Option>
              <Select.Option value="50">50</Select.Option>
              <Select.Option value="100">100</Select.Option>
              <Select.Option value="500">500</Select.Option>
            </Select>
            <span style={{ fontSize: 12 }}>entries</span>
          </div>
          <div style={{ marginLeft: "auto" }} className="flex align-center">
            <Search
              placeholder="Cari data"
              onSearch={(e) => {
                setParams({
                  name: e,
                });
                fetch(
                  {
                    name: e,
                  },
                  pagination
                );
              }}
            />
            <Button
              icon={<PlusOutlined />}
              type="primary"
              onClick={() => setOpen(true)}
              style={{ marginRight: 10 }}
            >
              Buat baru
            </Button>
          </div>
        </div>
      </Card>
      <Table
        style={{ marginTop: 10 }}
        columns={columns({
          fetch: () => fetch(params, pagination),
          setOpen: (data) => setOpen(data),
          setCurrent: (data) => setCurrent(data),
        })}
        rowKey={(record) => record.id}
        dataSource={data}
        pagination={pagination}
        loading={loading}
        onChange={handleTableChange}
      />
      <Drawer
        title="Kelola Kategori"
        onClose={() => {
          setOpen(false);
          setCurrent(false);
          fetch(params, pagination);
        }}
        visible={open}
      >
        <CategoryForm
          data={current}
          handleClose={() => {
            setOpen(false);
            setCurrent(false);
            fetch(params, pagination);
          }}
          fetch={() => fetch(params, pagination)}
        />
      </Drawer>
    </>
  );
}

export default TableCategory;
