import React from "react";
import Table from "antd/es/table";
import Button from "antd/es/button";
import Input from "antd/es/input";
import Card from "antd/es/card";
import Select from "antd/es/select";
import Spin from "antd/es/spin";
import Tooltip from "antd/es/tooltip";
import DatePicker from "antd/es/date-picker";
import Row from "antd/es/row";
import Col from "antd/es/col";
import Popconfirm from "antd/es/popconfirm";
import Tag from "antd/es/tag";
import {
  PrinterOutlined,
  PlusOutlined,
  SearchOutlined,
  DeleteOutlined,
  CheckOutlined,
} from "@ant-design/icons";
import ReactToPrint, { PrintContextConsumer } from "react-to-print";
import http from "../../utils/http";
import moment from "moment";
import TransactionForm from "../Forms/TransactionForm";
import PrintLayout from "../../layouts/PrintLayout";

const pageStyle = `
@media all {
  .page-break {
    display: none;
  }
}

@media print {
  html, body {
    height: initial !important;
    overflow: initial !important;
    -webkit-print-color-adjust: exact;
  }
}

@media print {
  .page-break {
    margin-top: 1rem;
    display: block;
    page-break-before: auto;
  }
}

@page {
  size: auto;
  margin: 10mm;
}
`;
const columns = (props) => [
  {
    title: "Tanggal",
    dataIndex: "created_at",
    render: (text, record, index) =>
      moment(new Date(record.created_at)).format("DD MMM YYYY HH:mm:ss"),
    responsive: ["lg"],
  },
  {
    title: "Kode pinjam",
    dataIndex: "transaction_number",
  },
  {
    title: "Nama peminjam",
    dataIndex: "name",
    responsive: ["lg"],
  },
  {
    title: "No kontak peminjam",
    dataIndex: "contact",
    responsive: ["lg"],
  },
  {
    title: "#",
    width: "10%",
    align: "center",
    dataIndex: "action",
    render: (text, record) => (
      <div style={{ display: "flex", alignItems: "center" }}>
        <ReactToPrint
          content={() => props.printComponent.current}
          onAfterPrint={() => {
            props.setIsPrint(false);
          }}
        >
          <PrintContextConsumer>
            {({ handlePrint }) => (
              <Tooltip title="Print Invoice">
                <Button
                  style={{ marginRight: 5 }}
                  icon={<PrinterOutlined />}
                  type="primary"
                  onClick={async () => {
                    await props.setIsPrint(true);
                    props.fetchDetail(
                      record.id,
                      handlePrint,
                      `${record.transaction_number}-${record.name}-${record.contact}`
                    );
                  }}
                >
                  Print Invoice
                </Button>
              </Tooltip>
            )}
          </PrintContextConsumer>
        </ReactToPrint>
        <Popconfirm
          placement="left"
          title="Anda yakin ingin menghapus data ini?"
          onConfirm={() => {
            const postData = { id: record.id };
            http({
              url: "/v1/transaction",
              method: "DELETE",
              data: postData,
            }).then(() => {
              props.fetch("");
            });
          }}
          okText="Iya"
          cancelText="Batal"
        >
          <Button type="danger" onClick={() => {}} icon={<DeleteOutlined />}>
            Hapus
          </Button>
        </Popconfirm>
      </div>
    ),
  },
];

const columnsPrint = (props) => [
  {
    title: "Alat",
    dataIndex: "inventory",
    render: (text, record) => record?.inventory?.name,
  },
  {
    title: "Qty",
    dataIndex: "qty",
  },
  {
    title: "Status",
    dataIndex: "status",
    render: (text, record, index) =>
      record?.status === 1 ? (
        <Tag color={"red"}>BELUM DIKEMBALIKAN</Tag>
      ) : (
        <Tag color={"green"}>SUDAH DIKEMBALIKAN</Tag>
      ),
    responsive: ["lg"],
  },
];

const columnsDetail = (props) => [
  {
    title: "Alat",
    dataIndex: "inventory",
    render: (text, record) => record?.inventory?.name,
  },
  {
    title: "Qty",
    dataIndex: "qty",
  },
  {
    title: "Status",
    dataIndex: "status",
    render: (text, record, index) =>
      record?.status === 1 ? (
        <Tag color={"red"}>BELUM DIKEMBALIKAN</Tag>
      ) : (
        <Tag color={"green"}>SUDAH DIKEMBALIKAN</Tag>
      ),
    responsive: ["lg"],
  },
  {
    title: "#",
    width: "10%",
    align: "center",
    dataIndex: "action",
    render: (text, record) => (
      <div style={{ display: "flex", alignItems: "center" }}>
        <Popconfirm
          placement="left"
          title="Barang sudah dikembalikan?"
          onConfirm={() => {
            const postData = { id: record.id };
            http({
              url: "/v1/transaction/return",
              method: "PUT",
              data: postData,
            }).then(() => {
              props.fetch("");
            });
          }}
          okText="Iya"
          cancelText="Batal"
        >
          <Button type="primary" onClick={() => {}} icon={<CheckOutlined />}>
            Pengembalian
          </Button>
        </Popconfirm>
      </div>
    ),
  },
];

function TableTransaction() {
  const printComponent = React.useRef();
  const [data, setData] = React.useState([]);
  const [params, setParams] = React.useState({
    name: "",
    date_start: "",
    date_end: "",
  });
  const [pagination, setPagination] = React.useState({
    current: 1,
    pageSize: 10,
    total: 0,
  });
  const [isPrint, setIsPrint] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [detail, setDetail] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [trxnum, setTrxnum] = React.useState("");
  const { Search } = Input;
  const { RangePicker } = DatePicker;
  React.useEffect(() => {
    fetch(params, pagination);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleTableChange = async (page) => {
    fetch(params, page);
  };

  const fetch = (data = params, pagination) => {
    setLoading(true);
    http
      .get(
        `/v1/transaction?transaction_number=${data.name}&start=${
          data.date_start ? moment(data.date_start).format("YYYY-MM-DD") : ""
        }&end=${
          data.date_end ? moment(data.date_end).format("YYYY-MM-DD") : ""
        }&page=${pagination?.current}&pageSize=${pagination?.pageSize}`
      )
      .then(async (data) => {
        await setLoading(false);
        await setData(data?.data?.serve.data);
        await setPagination({
          current: data?.data?.serve.current_page,
          pageSize: data?.data?.serve.per_page,
          total: data?.data?.serve.total,
        });
      });
  };

  const fetchDetail = (id, handlePrint, trx) => {
    http.get(`/v1/transaction/detail?id=${id}`).then(async (data) => {
      await setTrxnum(trx);
      await setDetail(data?.data?.serve);
      handlePrint();
    });
  };

  return (
    <Spin
      spinning={isPrint}
      tip="Memproses data, mohon tunggu.."
      className="h-full"
    >
      <style>{pageStyle}</style>
      <Card className="card-product-filter" style={{ marginTop: 10 }}>
        <Row
          gutter={[12, 12]}
          style={{ width: "100%" }}
          justify="center"
          align="middle"
        >
          <Col xs={24} sm={24} md={24} lg={2} xl={2}>
            <div style={{ fontSize: 12 }}>Filter Tanggal</div>
          </Col>
          <Col xs={24} sm={24} md={24} lg={19} xl={19}>
            <RangePicker
              value={
                params.date_start && params.date_end
                  ? [moment(params.date_start), moment(params.date_end)]
                  : ""
              }
              style={{ width: "100%" }}
              onChange={async (e, f) => {
                if (f.length > 0) {
                  setParams({
                    name: params.name,
                    date_start: moment(f[0]),
                    date_end: moment(f[1]),
                  });
                }
              }}
            />
          </Col>
          <Col xs={24} sm={24} md={24} lg={2} xl={2}>
            <Button
              icon={<SearchOutlined />}
              type="primary"
              onClick={() => {
                fetch(params, pagination);
              }}
              style={{ width: "100%" }}
            >
              Cari
            </Button>
          </Col>
          <Col xs={24} sm={24} md={24} lg={1} xl={1}>
            <Button
              type="link"
              onClick={() => {
                setParams({
                  name: "",
                  date_start: "",
                  date_end: "",
                });
                fetch(
                  {
                    name: "",
                    date_start: "",
                    date_end: "",
                  },
                  pagination
                );
              }}
            >
              Reset
            </Button>
          </Col>
        </Row>
      </Card>
      <Card className="card-product-filter" style={{ marginTop: 10 }}>
        <div
          className="flex flex-wrap"
          style={{ width: "100%", alignItems: "flex-end" }}
        >
          <div className="flex align-center">
            <span style={{ fontSize: 12 }}>Show</span>
            <Select
              defaultActiveFirstOption={false}
              onChange={(e) => {
                setPagination({
                  current: pagination.current,
                  pageSize: e,
                  total: pagination.total,
                });
                fetch(params, {
                  current: pagination.current,
                  pageSize: e,
                  total: pagination.total,
                });
              }}
              style={{ width: "60px", marginLeft: 10, marginRight: 10 }}
              value={pagination.pageSize}
            >
              <Select.Option value="10">10</Select.Option>
              <Select.Option value="50">50</Select.Option>
              <Select.Option value="100">100</Select.Option>
              <Select.Option value="500">500</Select.Option>
            </Select>
            <span style={{ fontSize: 12 }}>entries</span>
          </div>
          <div style={{ marginLeft: "auto" }} className="flex align-center">
            <Search
              placeholder="Cari berdasar kode pinjam"
              onSearch={(e) => {
                setParams({
                  date_end: params.date_end,
                  date_start: params.date_start,
                  name: e,
                });
                fetch(
                  {
                    date_end: params.date_end,
                    date_start: params.date_start,
                    name: e,
                  },
                  pagination
                );
              }}
            />
            <Button
              icon={<PlusOutlined />}
              type="primary"
              onClick={() => setOpen(true)}
              style={{ marginRight: 10 }}
            >
              Buat baru
            </Button>
          </div>
        </div>
      </Card>
      {open ? (
        <Card className="card-product-filter" style={{ marginTop: 10 }}>
          <TransactionForm
            handleClose={() => {
              setOpen(false);
              fetch(params, pagination);
            }}
          />
        </Card>
      ) : null}
      {isPrint ? (
        <div ref={printComponent}>
          <PrintLayout title="INVOICE" subtitle={`Kode Unik #${trxnum}`}>
            <Table
              columns={columnsPrint({
                isPrint: isPrint,
                fetch: () => fetch(params, pagination),
              })}
              rowKey={(record) => record.id}
              dataSource={detail}
              pagination={false}
              loading={loading}
              onChange={handleTableChange}
              summary={(pageData) => {
                let totalQty = 0;

                pageData.forEach(({ qty }) => {
                  totalQty += parseInt(qty);
                });

                return (
                  <>
                    <Table.Summary.Row>
                      <Table.Summary.Cell align="right">
                        Total Qty
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={3}>
                        <div>{totalQty}</div>
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                  </>
                );
              }}
            />
          </PrintLayout>
        </div>
      ) : null}
      <Table
        style={{ marginTop: 10 }}
        columns={columns({
          fetch: () => fetch(params, pagination),
          fetchDetail: (data, print, trx) => fetchDetail(data, print, trx),
          setIsPrint: (data) => setIsPrint(data),
          printComponent: printComponent,
        })}
        rowKey={(record) => record.id}
        dataSource={data}
        pagination={pagination}
        loading={loading}
        onChange={handleTableChange}
        expandable={{
          expandedRowRender: (record) => (
            <Table
              columns={columnsDetail({
                isPrint: isPrint,
                fetch: () => fetch(params, pagination),
              })}
              rowKey={(record) => record.id}
              dataSource={record.details}
              pagination={false}
              loading={loading}
              summary={(pageData) => {
                let totalQty = 0;

                pageData.forEach(({ qty }) => {
                  totalQty += parseInt(qty);
                });

                return (
                  <>
                    <Table.Summary.Row>
                      <Table.Summary.Cell align="right">
                        Total Qty
                      </Table.Summary.Cell>
                      <Table.Summary.Cell colSpan={3}>
                        <div>{totalQty}</div>
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                  </>
                );
              }}
            />
          ),
        }}
      />
    </Spin>
  );
}

export default TableTransaction;
