# Dockerfile
FROM node:16-alpine

# create destination directory
RUN mkdir -p /usr/src/nuxt-app
WORKDIR /usr/src/nuxt-app

# update and install dependency
#RUN apk update && apk upgrade
RUN apk add git

# copy the app, note .dockerignore
COPY . /usr/src/nuxt-app/
RUN yarn install
RUN npm run prebuild
RUN npm run build:prod

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

CMD [ "npm", "start" ]

# FROM node:16-alpine AS builder

# WORKDIR /app

# COPY . /app

# RUN npm install && npm run build

# ###################

# FROM node:16-alpine

# WORKDIR /app

# COPY --from=builder /app/.nuxt /app/.nuxt
# COPY --from=builder /app/node_modules /app/node_modules
# COPY --from=builder /app/static /app/static
# COPY --from=builder /app/package.json /app/package-lock.json /app/nuxt.config.js /app/

# CMD ["npm","start"]
